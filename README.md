# training_and_experiments

Tutorials, python walkthroughs, and examples of various topics. I put this together to make it easier to remember specific things I have done over the years and put them up. However examples are availible for anyone

# Contents:

## General Python
* [Functions vs Object Oriented Programming](walkthroughs/Functions_vs_Object_Oriented_Programming.ipynb)
* [How class (object) inheritance works in Python](walkthroughs/classes_and_inheritance.ipynb)
* [Asynchronous (multi) processing in Python](walkthroughs/parallel_programming_tutorial.ipynb)

## Network Analysis
* [Creating visuals of graphs (using *igraph.plot()*)](walkthroughs/Visualizing_netwroks_in_igraph.ipynb)

## Good external resources:
* [Cheatsheet on pandas DataFrame processing (external)](https://www.dataquest.io/blog/large_files/pandas-cheat-sheet.pdf)